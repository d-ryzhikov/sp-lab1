#include <Windows.h>
#include "resource.h"

LPCWSTR g_szClassName = L"sp_lab1";
const int ID_TIMER = 1;

struct FlyingText
{
    int x = 0;
    int y = 0;

    int width;
    int height;

    int dx = 2; //horizontal speed
    int dy = 2; //vertical speed

    LPCWSTR text = L"TOP KEK!";

    bool is_moving = true;
} flying_text;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

void UpdatePosition(RECT* rect);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hwnd;
    MSG msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 0;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON));
    wc.hCursor = LoadCursor(GetModuleHandle(NULL), MAKEINTRESOURCE(IDC_CURSOR));
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU);
    wc.lpszClassName = g_szClassName;
    wc.hIconSm = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON));

    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, L"Window registration failed!", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    hwnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        g_szClassName,
        L"������������ ������ �1",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, 600, 400,
        NULL, NULL, hInstance, NULL);

    if(hwnd == NULL)
    {
        MessageBox(NULL, L"Window creation failed!", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    while(GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        case WM_CREATE:
        {
            if(!SetTimer(hwnd, ID_TIMER, 100, NULL))
            {
                MessageBox(hwnd, L"Couldn't create a timer.", L"Error!",
                    MB_ICONEXCLAMATION | MB_OK);
                return FALSE;
            }

            HDC hdc = GetDC(hwnd);
            SIZE size;

            GetTextExtentPoint32(hdc, flying_text.text, lstrlen(flying_text.text), &size);
            ReleaseDC(hwnd, hdc);
            flying_text.width = size.cx;
            flying_text.height = size.cy;
            break;
        }
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case ID_SUBMENU_START:
                    flying_text.is_moving = TRUE;
                    break;
                case ID_SUBMENU_STOP:
                    flying_text.is_moving = false;
                    break;
            }
            break;
        case WM_PAINT:
        {
            HDC hdc;
            PAINTSTRUCT paintStruct;

            hdc = BeginPaint(hwnd, &paintStruct);
            TextOut(hdc, flying_text.x, flying_text.y, flying_text.text,
                lstrlen(flying_text.text));
            EndPaint(hwnd, &paintStruct);
            break;
        }
        case WM_TIMER:
        {
            HDC hdc = GetDC(hwnd);
            RECT rect;

            GetClientRect(hwnd, &rect);
            UpdatePosition(&rect);
            InvalidateRect(hwnd, NULL, TRUE);   //trigger WM_PAINT
            ReleaseDC(hwnd, hdc);
            break;
        }
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            KillTimer(hwnd, ID_TIMER);
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

void UpdatePosition(RECT* rect)
{
    if (flying_text.is_moving)
    {
        flying_text.x += flying_text.dx;
        flying_text.y += flying_text.dy;

        if (flying_text.x < 0)
        {
            flying_text.x = 0;
            flying_text.dx *= -1;
        }
        else if (flying_text.x+flying_text.width > rect->right)
        {
            flying_text.x = rect->right - flying_text.width;
            flying_text.dx *= -1;
        }

        if (flying_text.y < 0)
        {
            flying_text.y = 0;
            flying_text.dy *= -1;
        }
        else if (flying_text.y+flying_text.height > rect->bottom)
        {
            flying_text.y = rect->bottom - flying_text.height;
            flying_text.dy *= -1;
        }
    }
}
