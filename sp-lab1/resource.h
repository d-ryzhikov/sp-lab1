//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется sp-lab1.rc
//
#define IDI_ICON                        102
#define IDR_MENU                        103
#define IDC_CURSOR                      104
#define ID_MENU_SUBMENU                 40007
#define ID_SUBMENU_START                40008
#define ID_SUBMENU_STOP                 40009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40010
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
